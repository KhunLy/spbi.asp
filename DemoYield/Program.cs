﻿using Movies.DAL.Entities;
using Movies.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoYield
{
    class Program
    {
        static void Main(string[] args)
        {
            ActorRepository repo = new ActorRepository();
            IEnumerable<Actor> actors = repo.Get();
            foreach (Actor item in actors)
            {
                Console.WriteLine(item.FirstName);
            }
            Console.ReadKey();
        }
    }
}
