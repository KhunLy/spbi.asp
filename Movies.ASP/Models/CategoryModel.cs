﻿using Movies.ASP.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Movies.ASP.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        // Requis
        // Max Length 50
        [Required(ErrorMessage = "Le message que je vx")]
        [MaxLength(50, ErrorMessage = "....")]
        [CategoryUniqueName(ErrorMessage = "Flute")]
        public string Name { get; set; }

        // Max Length 255
        [MaxLength(255)]
        public string Description { get; set; }

        public IEnumerable<MovieModel> Movies { get; set; }
    }
}