﻿using Movies.DAL.Entities;
using Movies.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Movies.ASP.Models
{
    public class MovieModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }


        [MaxLength(65535)]
        public string Synopsis { get; set; }

        [Range(1900, 2050)]
        public int? ReleaseYear { get; set; }
        public string PosterUri { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public IEnumerable<CastModel> Casting { get; set; }

        #region Lazy Loading
        private IEnumerable<CategoryModel> _allCategories;
        public IEnumerable<CategoryModel> AllCategories
        {
            get
            {
                if (_allCategories == null)
                {
                    CategoryRepository repo = new CategoryRepository();
                    _allCategories = repo.Get().Select(c => new CategoryModel
                    {
                        Id = c.Id,
                        Name = c.Name
                    });
                }
                return _allCategories;
            }
        } 
        #endregion
    }
}