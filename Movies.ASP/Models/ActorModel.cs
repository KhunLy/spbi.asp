﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Movies.ASP.Models
{
    public class ActorModel
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string ImageUri { get; set; }
    }
}