﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Movies.ASP.Models
{
    public class CastModel
    {
        public int MovieId { get; set; }
        public int ActorId { get; set; }
        public string CharacterName { get; set; }
        public string ActorLastName { get; set; }
        public string ActorFirstName { get; set; }
    }
}