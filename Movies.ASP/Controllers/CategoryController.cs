﻿using Movies.ASP.Models;
using Movies.DAL.Entities;
using Movies.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.IO;

namespace Movies.ASP.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            ViewBag.Item = "42";
            CategoryRepository repo = new CategoryRepository();
            IEnumerable<Category> cats = repo.Get();
            IEnumerable<CategoryModel> model = cats.Select(c => new CategoryModel {
                Id = c.Id,
                Name = c.Name,
                Description = c.Description
            });
            return View(model);
        }

        public ActionResult Details(int id)
        {
            CategoryRepository repo = new CategoryRepository();
            MovieRepository repo2 = new MovieRepository();
            Category c = repo.GetById(id);
            CategoryModel model = new CategoryModel {
                Id = c.Id,
                Name = c.Name,
                Description = c.Description,
                Movies = repo2.GetByCategory(id).Select(m => new MovieModel {
                    Title = m.Title
                })
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            CategoryModel model = new CategoryModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CategoryModel model)
        {
            if (ModelState.IsValid)
            {
                // Enregistrer la categorie dans la db
                CategoryRepository repo = new CategoryRepository();
                try
                {
                    repo.Insert(new Category { Name = model.Name, Description = model.Description });
                } catch (Exception e)
                {
                    // Message
                    ViewBag.Error = "message bidon";
                    //string path = HostingEnvironment.MapPath("/Logs/test.txt");
                    //using (StreamWriter w = System.IO.File.AppendText(path))
                    //{
                    //    w.WriteLine(e.Message);
                    //}
                    return View(model);
                }
                // Redirection vers l'index
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CategoryRepository repo = new CategoryRepository();
            bool success = repo.Delete(id);
            if (success)
                TempData["success"] = "Suppression OK";
            else
                TempData["error"] = "Une erreur est survenue";
            return RedirectToAction("Index");
        }
    }
}