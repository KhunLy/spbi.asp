﻿using Movies.ASP.Models;
using Movies.DAL.Entities;
using Movies.DAL.Repositories;
using Movies.DAL.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Movies.ASP.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie
        public ActionResult Index()
        {
            MovieRepository repo = new MovieRepository();
            IEnumerable<Movie> movies = repo.Get();
            IEnumerable<MovieModel> model = movies.Select(m => new MovieModel {
                Id = m.Id,
                Title = m.Title,
                Synopsis = m.Synopsis
            });
            return View(model);
        }

        public ActionResult Details(int id)
        {
            MovieRepository repo = new MovieRepository();
            CastRepository repo2 = new CastRepository();
            VMovieCategory m = repo.GetWithCategory(id);
            MovieModel model = new MovieModel {
                Id = m.Id,
                Title = m.Title,
                Synopsis = m.Synopsis,
                ReleaseYear = m.ReleaseYear,
                CategoryName = m.CategoryName,
                Casting = repo2.GetCastingByMovie(id).Select(c => new CastModel {
                    ActorFirstName = c.ActorFirstName,
                    ActorLastName = c.ActorLastName,
                    CharacterName = c.CharacterName
                })
            };
            return View(model);
        }
        
        public ActionResult Create()
        {
            MovieModel model = new MovieModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(MovieModel model)
        {
            if (ModelState.IsValid)
            {
                MovieRepository repo = new MovieRepository();
                CategoryRepository repo2 = new CategoryRepository();
                Category cat = repo2.Get().FirstOrDefault(c => c.Name == model.CategoryName);
                if (cat == null)
                {
                    cat = new Category { Name = model.CategoryName };
                    int id = repo2.Insert(cat);
                    cat.Id = id;
                }
                repo.Insert(new Movie { 
                    Title = model.Title,
                    Synopsis = model.Synopsis,
                    ReleaseYear = model.ReleaseYear,
                    CategoryId = cat.Id
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        //public bool IsValid(object o)
        //{
        //    Type t = o.GetType();
        //    IEnumerable<PropertyInfo> props =  t.GetProperties();
        //    foreach (var item in props)
        //    {
        //        IEnumerable<ValidationAttribute> attr = item.GetCustomAttributes()
        //            .Where(a => a is ValidationAttribute)
        //            .Cast<ValidationAttribute>();
        //        object value = item.GetValue(o);
        //        foreach(ValidationAttribute a in attr)
        //        {
        //            if(!a.IsValid(value))
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    return true;

        //} 
    }
}