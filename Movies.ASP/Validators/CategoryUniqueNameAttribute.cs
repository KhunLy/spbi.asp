﻿using Movies.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Movies.ASP.Validators
{
    public class CategoryUniqueNameAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null) return true;
            string name = value as string;
            CategoryRepository repo = new CategoryRepository();
            return repo.Get().Count(c => c.Name == name) == 0;
        }
    }
}