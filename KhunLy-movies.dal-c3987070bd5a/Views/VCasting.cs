﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Views
{
    public class VCasting
    {
        public int MovieId { get; set; }
        public int ActorId { get; set; }
        public string CharacterName { get; set; }
        public string MovieTitle { get; set; }
        public string ActorLastName { get; set; }
        public string ActorFirstName { get; set; }
        public DateTime? ActorBirthDate { get; set; }
        public string ActorImageUri { get; set; }
    }
}
