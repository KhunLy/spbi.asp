﻿using Movies.DAL.Entities;
using Movies.DAL.Mappers;
using Movies.DAL.Views;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Repositories
{
    public class MovieRepository: BaseRepository
    {
        #region Constructors
        public MovieRepository() : base() { }
        public MovieRepository(string connectionString) : base(connectionString) { }
        #endregion

        #region Methods
        /// <summary>
        /// Retourne tous les films de la db trié par titre
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Movie> Get()
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Movie] ORDER BY title";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader.MapToMovie();
                }
            }
        }

        /// <summary>
        /// Retourne un certain nombre de films de la db à partir d'un certain index 
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IEnumerable<Movie> Get(int limit, int offset)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Movie] ORDER BY title ASC " +
                    "OFFSET @offset ROWS " +
                    "FETCH NEXT @limit ROWS ONLY";
                command.Parameters.AddWithValue("@offset", offset);
                command.Parameters.AddWithValue("@limit", limit);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader.MapToMovie();
                }
            }
        }

        /// <summary>
        /// Retourne le film dont l'id est l'id passé en paramètre ou null si aucun film n'est trouvé"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Movie GetById(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Movie] WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    return reader.MapToMovie();
                }
                return null;
            }
        }

        /// <summary>
        /// Insère un film dans la table Movie et retourne l'id du film inséré
        /// </summary>
        /// <returns></returns>
        public int Insert(Movie m)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "INSERT INTO [Movie]([title], [synopsis], [release_year], [poster_uri], [category_id]) " +
                    "OUTPUT INSERTED.id VALUES (@title, @synopsis, @release_year, @poster_uri, @category_id)";
                command.Parameters.AddWithValue("@title", m.Title);
                command.Parameters.AddWithValue("@synopsis", (object)m.Synopsis ?? DBNull.Value);
                command.Parameters.AddWithValue("@release_year", (object)m.ReleaseYear ?? DBNull.Value);
                command.Parameters.AddWithValue("@poster_uri", (object)m.PosterUri ?? DBNull.Value);
                command.Parameters.AddWithValue("@category_id", (object)m.CategoryId ?? DBNull.Value);
                return (int)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Met à jour une ligne de la table Movie
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public bool Update(Movie m)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE [Movie] SET " +
                    "[title] = @title, " +
                    "[synopsis] = @synopsis, " +
                    "[release_year] = @release_year, " +
                    "[poster_uri] = @poster_uri, " +
                    "[category_id] = @category_id " +
                    "WHERE id = @id";
                command.Parameters.AddWithValue("@title", m.Title);
                command.Parameters.AddWithValue("@synopsis", (object)m.Synopsis ?? DBNull.Value);
                command.Parameters.AddWithValue("@release_year", (object)m.ReleaseYear ?? DBNull.Value);
                command.Parameters.AddWithValue("@poster_uri", (object)m.PosterUri ?? DBNull.Value);
                command.Parameters.AddWithValue("@category_id", (object)m.CategoryId ?? DBNull.Value);
                command.Parameters.AddWithValue("@id", m.Id);
                return command.ExecuteNonQuery() == 1;
            }
        }

        /// <summary>
        /// Supprime un film de la table Movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "DELETE FROM [Movie] WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery() == 1;
            }
        }

        /// <summary>
        /// Retourne les infos et la categorie du film dont l'id est l'id passé en paramètre ou null si aucun film n'est trouvé"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public VMovieCategory GetWithCategory(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [V_Movie_Category] WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    return reader.MapToVMovieCategory();
                }
                return null;
            }
        }

        /// <summary>
        /// Retourne la liste de tous les films d'une certaine categorie
        /// </summary>
        /// <param name="catId"></param>
        /// <returns></returns>
        public IEnumerable<Movie> GetByCategory(int catId)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Movie] " +
                    "WHERE category_id = @cat_id " +
                    "ORDER BY title";
                command.Parameters.AddWithValue("@cat_id", catId);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader.MapToMovie();
                }
            }
        } 
        #endregion
    }
}
