﻿using Movies.DAL.Entities;
using Movies.DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Repositories
{
    public class ActorRepository : BaseRepository
    {
        #region Constructors
        public ActorRepository() : base() { }

        public ActorRepository(string connectionString) : base(connectionString) { }
        #endregion

        #region Methods
        public IEnumerable<Actor> Get() 
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Actor] ORDER BY [last_name]";
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    yield return reader.MapToActor();
                }
            }
        }
        public IEnumerable<Actor> Get(int limit, int offset)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Actor] ORDER BY [last_name] " +
                    "OFFSET @offset ROWS " +
                    "FETCH NEXT @limit ROWS ONLY";
                command.Parameters.AddWithValue("@limit", limit);
                command.Parameters.AddWithValue("@offset", offset);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader.MapToActor();
                }
            }
        }
        public Actor GetById(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Actor] ORDER BY [last_name] " +
                    "WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    return reader.MapToActor();
                }
                return null;
            }
        }
        public int Insert(Actor a)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "INSERT INTO [Actor]([last_name],[first_name],[birth_date],[image_uri]) " +
                    "OUTPUT Inserted.id VALUES " +
                    "(@last_name,@first_name,@birth_date,@image_uri)";
                command.Parameters.AddWithValue("@last_name", a.LastName);
                command.Parameters.AddWithValue("@first_name", a.FirstName);
                command.Parameters.AddWithValue("@birth_date", (object)a.BirthDate ?? DBNull.Value);
                command.Parameters.AddWithValue("@image_uri", (object)a.ImageUri ?? DBNull.Value);
                return (int)command.ExecuteScalar();
            }
        }
        public bool Update(Actor a)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE [Actor] SET " +
                    "last_name = @last_name, " +
                    "first_name = @first_name, " +
                    "birth_date = @birth_date, " +
                    "image_uri = @image_uri " +
                    "WHERE id = @id";
                command.Parameters.AddWithValue("@last_name", a.LastName);
                command.Parameters.AddWithValue("@first_name", a.FirstName);
                command.Parameters.AddWithValue("@birth_date", (object)a.BirthDate ?? DBNull.Value);
                command.Parameters.AddWithValue("@image_uri", (object)a.ImageUri ?? DBNull.Value);
                command.Parameters.AddWithValue("@id", a.Id);
                return command.ExecuteNonQuery() == 1;
            }
        }
        public bool Delete(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "DELETE FROM [Actor] WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery() == 1;
            }
        }
        #endregion
    }
}
