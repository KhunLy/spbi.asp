﻿using Movies.DAL.Entities;
using Movies.DAL.Mappers;
using Movies.DAL.Views;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Repositories
{
    public class CastRepository: BaseRepository
    {
        #region Constructors
        public CastRepository() : base() { }

        public CastRepository(string connectionString) : base(connectionString) { }
        #endregion

        #region Methods
        public IEnumerable<VCasting> GetCastingByMovie(int movieId)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [V_Casting] WHERE movie_id = @id";
                command.Parameters.AddWithValue("@id", movieId);
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    yield return reader.MapToVCasting();
                }
            }
        }

        public bool Insert(Cast c)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "INSERT INTO [Cast]([movie_id], [actor_id], [character_name]) VALUES (@movie_id, @actor_id, @character_name)";
                command.Parameters.AddWithValue("@character_name", c.CharacterName);
                command.Parameters.AddWithValue("@movie_id", c.MovieId);
                command.Parameters.AddWithValue("@actor_id", c.ActorId);
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool Update(Cast c)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE [Cast] SET [character_name] = @character_name " +
                    "WHERE movie_id = @movie_id AND actor_id = @actor_id";
                command.Parameters.AddWithValue("@character_name", c.CharacterName);
                command.Parameters.AddWithValue("@movie_id", c.MovieId);
                command.Parameters.AddWithValue("@actor_id", c.ActorId);
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool Delete(int movieId, int actorId)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "DELETE FROM [Cast] " +
                    "WHERE movie_id = @movie_id AND actor_id = @actor_id";
                command.Parameters.AddWithValue("@movie_id", movieId);
                command.Parameters.AddWithValue("@actor_id", actorId);
                return command.ExecuteNonQuery() == 1;
            }
        }
        #endregion
    }
}
