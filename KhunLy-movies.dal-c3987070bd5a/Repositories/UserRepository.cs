﻿using Movies.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Repositories
{
    public class UserRepository: BaseRepository
    {
        #region Constructors
        public UserRepository() : base() { }
        public UserRepository(string connectionString) : base(connectionString) { } 
        #endregion


    }
}
